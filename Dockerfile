# Select a base image from which to extend
FROM gitlab-registry.cern.ch/mdraguet/salt:latest

# common environemnt variables
ENV NB_USER mdraguet
ENV NB_UID 1000
ENV NB_PREFIX /
ENV HOME /home/$NB_USER
ENV SHELL /bin/bash

# args - software versions
ARG KUBECTL_ARCH="amd64"
ARG KUBECTL_VERSION=v1.21.1
 # renovate: datasource=github-tags depName=just-containers/s6-overlay versioning=loose
# set shell to bash
SHELL ["/bin/bash", "-c"]

# install - usefull linux packages
RUN export DEBIAN_FRONTEND=noninteractive \
 && apt-get -yq update \
 && apt-get -yq install --no-install-recommends \
    apt-transport-https \
    bash \
    bzip2 \
    ca-certificates \
    curl \
    openssh-client \
    git \
    gnupg \
    gnupg2 \
    locales \
    lsb-release \
    nano \
    vim \
    emacs \
    software-properties-common \
    tzdata \
    unzip \
    wget \
    zip \
    tree \
    libpam-krb5 \
    krb5-user \
    python3.8 \
    python-is-python3 \
    pip \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && pip install --upgrade pip

# install - kubectl
RUN curl -sL "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/${KUBECTL_ARCH}/kubectl" -o /usr/local/bin/kubectl \
 && curl -sL "https://dl.k8s.io/${KUBECTL_VERSION}/bin/linux/${KUBECTL_ARCH}/kubectl.sha256" -o /tmp/kubectl.sha256 \
 && echo "$(cat /tmp/kubectl.sha256) /usr/local/bin/kubectl" | sha256sum --check \
 && rm /tmp/kubectl.sha256 \
 && chmod +x /usr/local/bin/kubectl

# create user and set required ownership
RUN useradd -M -s /bin/bash -N -u ${NB_UID} ${NB_USER} \
 && groupadd -g 1337 istio \
 && mkdir -p ${HOME} \
 && chown -R ${NB_USER}:users ${HOME} \
 && chown -R ${NB_USER}:users /usr/local/bin \
 && echo 'PS1="${debian_chroot:+($debian_chroot)}@\h:\w\$ "' >> /etc/bash.bashrc

# set locale configs
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
 && locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV PATH /bin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/sbin:/bin:/opt/nvidia-driver/bin:/opt/conda/bin
ENV LD_LIBRARY_PATH /opt/nvidia-driver/lib64
 
# setup cern kerberos
COPY krb5.conf /etc/krb5.conf

# Seems required for correct install
RUN pip install jsonschema==3.0.1
RUN pip install google-auth==2.14.1

# Also setup kpf
RUN pip install kfp==1.8.1

USER root

# Install required packages
RUN python -m pip install jupyterlab

# Let's give ownership of the /salt folder to the default user: mdraguet 
RUN chown mdraguet: /salt

USER mdraguet

CMD ["sh", "-c", \
    "jupyter lab --notebook-dir=/home/mdraguet --ip=0.0.0.0 --no-browser \
    --allow-root --port=8888 --LabApp.token='' --LabApp.password='' \
    --LabApp.allow_origin='*' --LabApp.base_url=${NB_PREFIX}"]